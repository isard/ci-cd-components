# CI CD Components

## TODO
- Linter for BASH
- Linter for Docker
- Linter for Docker Compose
- Linter for Terraform Modules
- Preview CHANGELOG
- Centralize the linter configuration somehow

## Repository functionality

### Commits

Description: lints the commints, ensuring they follow conventionalcommits
Steps: `lint`

### Release

Description: releases new versions using conventionalcommits as base for semantic versioning
Steps: `lint`, `release`
Inputs:
  - `extra_command`: [Default: ``]: A command that will be executed right before the release
Generates: `.VERSION`, which contains the version that is going to be released

## Language specific

### Ansible

Description: lints ansible files
Steps: `lint`

### Go

Description: runs unit tests and reports the coverage and tests
Steps: `unit-test`

### Javascript

Description: runs `yarn lint`
Steps: `lint`
Inputs:
  - `project_directory`: [Default: `.`]: The directory where the project lives, relative to the root of the repository

### ProtoBuf

Description: lints protocol buffers files and checks for breaking changes using `buf`
Steps: `lint`

### Python

Description: lints python files, using various linters and configuration checkers
Steps: `lint`
